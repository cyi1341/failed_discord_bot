import nextcord

class failed_discord_bot:
    def __init__(self):
        self.intents = nextcord.Intents.default()
        self.intents.message_content = True
        self.bot = nextcord.Client(intents=self.intents)

    def get_bot(self):
        return self.bot

    def get_intents(self):
        return self.intents
