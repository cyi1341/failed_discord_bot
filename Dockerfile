# Use Alpine 3.11 as base image
FROM python:3.11-alpine

# Install dependencies
COPY requirements.txt ./

RUN pip install -r requirements.txt

# Set working directory 
WORKDIR /app

# Bundle app source
COPY . .

# Expose port 
EXPOSE 8080  

# Run bot
CMD ["python", "bot.py"]
